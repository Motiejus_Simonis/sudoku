package Main;

import SudokuApp.Sudoku;
import SudokuApp.SudokuApp;

public class MainSudoku {
	public static void main(String[] args) {
		javafx.application.Application.launch(SudokuApp.class);
		
		Sudoku sudoku = new Sudoku();
		
		int[][] sudoku1 = {
				{1, 5, 2,   4, 6, 9,   3, 7, 8},
				{7, 8, 9,   2, 1, 3,   4, 5, 6},
				{4, 3, 6,   5, 8, 7,   2, 9, 1},
				
				{6, 1, 3,   8, 7, 2,   5, 4, 9},
				{9, 7, 4,   1, 5, 6,   8, 2, 3},
				{8, 2, 5,   9, 3, 4,   1, 6, 7},
				
				{5, 6, 7,   3, 4, 8,   9, 1, 2},
				{2, 4, 8,   6, 9, 1,   7, 3, 5},
				{3, 9, 1,   7, 2, 5,   6, 8, 4}
		};
		
		sudoku.setSudoku(sudoku1);
		System.out.println(sudoku.checkColums());
		System.out.println(sudoku.checkRows());
		System.out.println(sudoku.checkSqueres());
		System.out.println(sudoku.checkSudoku());
		
		int[][] test = new int[9][9];
		
		System.out.println(test[1][1]);
	}
}

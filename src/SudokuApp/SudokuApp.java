package SudokuApp;

import java.util.HashMap;
import java.util.Map;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class SudokuApp extends Application {
	private Scene sceneRoot;
	private Sudoku sudoku = new Sudoku();


	@Override
	public void start(Stage primaryStage) throws Exception {

		sceneRoot = sceneRoot();

		primaryStage.setTitle("Motiejus-Sudoku");
		primaryStage.setScene(sceneRoot);
		primaryStage.show();
	}


	private Scene sceneRoot() {
		GridPane nineOnNine = new GridPane();
		nineOnNine.setPadding(new Insets(5, 5, 5, 5));
		nineOnNine.setAlignment(Pos.CENTER);
		nineOnNine.setHgap(0);
		nineOnNine.setVgap(0);

		Label dynamic = new Label("");
		
		Map<String,TextField> textFieldMap = new HashMap<String,TextField>();

		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				textFieldMap.put("" + i + j ,new TextField());
				System.out.println(""+i+j);
			}
		}
		
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				textFieldMap.get("" + i + j).setPrefHeight(50);
				textFieldMap.get("" + i + j).setPrefWidth(50);
				nineOnNine.add(textFieldMap.get("" + i + j), i , j);
			}
		}
		

		Button btn2 = new Button("Submit");
		btn2.setMinWidth(100);
		btn2.setOnAction(event -> {
			
			int[][] fromFields = new int[9][9];
			for (int i = 0; i < fromFields.length; i++) {
				for (int j = 0; j < fromFields[i].length; j++) {
					if(parseFromTextField(textFieldMap.get("" + i + j)) != -1) {
						fromFields[i][j] = parseFromTextField(textFieldMap.get("" + i + j));
					}	
				}
			} 
			sudoku.setSudoku(fromFields);
			if (sudoku.checkSudoku()) dynamic.setText("Success");
			
		});
		
		Button btn1 = new Button("Start new game");
		btn1.setMinWidth(100);
		btn1.setOnAction(event -> {
			
			int[][] sudoku1 = {
					{1, 5, 2,   4, 6, 9,   3, 7, 8},
					{7, 8, 9,   2, 1, 3,   4, 5, 6},
					{4, 3, 6,   5, 8, 7,   2, 9, 1},
					
					{6, 1, 3,   8, 7, 2,   5, 4, 9},
					{9, 7, 4,   1, 5, 6,   8, 2, 3},
					{8, 2, 5,   9, 3, 4,   1, 6, 7},
					
					{5, 6, 7,   3, 4, 8,   9, 1, 2},
					{2, 4, 8,   6, 9, 1,   7, 3, 5},
					{3, 9, 1,   7, 2, 5,   6, 8, 4}
			};
			for (int i = 0; i < sudoku1.length; i++) {
				for (int j = 0; j < sudoku1[i].length; j++) {
					if (sudoku1[i][j] > 0 && sudoku1[i][j] <= 9) {
						textFieldMap.get("" + i + j).setText("" + sudoku1[i][j]);
					}
				}
			}
		});
		

		GridPane bottomG = new GridPane();
		bottomG.setAlignment(Pos.CENTER);
		bottomG.setHgap(100);
		bottomG.setVgap(20);
		bottomG.add(btn1, 0, 0);
		bottomG.add(btn2, 1, 0);
		bottomG.add(dynamic, 0, 1);
		
		GridPane layout = new GridPane();
		layout.setAlignment(Pos.CENTER);
		layout.setHgap(0);
		layout.setVgap(20);
		layout.add(nineOnNine, 0, 0);
		layout.add(bottomG, 0, 1);
		
		

		sceneRoot = new Scene(layout, 800, 800);
//		sceneRoot.getStylesheets().add("/style/style.css");
		return sceneRoot;
	}


	private int parseFromTextField(TextField temp) {
		try {
			return Integer.parseInt(temp.getText());
		} catch (Exception e) {
			return -1;
		}	
	}
}

package SudokuApp;

public class Sudoku {

	int[][] sudokuArray;
	
	public boolean sudokuValues() {
		for (int i = 0; i < sudokuArray.length; i++) {
			for (int j = 0; j < sudokuArray[i].length; j++) {
				if (sudokuArray[i][j] > 9 && sudokuArray[i][j] < 1) return false;
			}
		}
		return true;
	}

	public void setSudoku(int[][] sudokuArray) {
		if (sudokuFull(sudokuArray) == true)
			this.sudokuArray = sudokuArray;
	}

	public boolean sudokuFull(int[][] sudokuArray) {
		if (sudokuArray.length != 9) {
			return false;
		}
		for (int i = 0; i < sudokuArray.length; i++) {
			if (sudokuArray[i].length != 9) {
				return false;
			}
		}
		return true;
	}

	public boolean checkSudoku() {
		return (checkSqueres() == true && checkRows() == true && checkColums() == true) ? true : false;
	}

	// ****************** ******************
	// *xy00, xy01, xy02* *xy03, xy04, xy05*
	// *xy10, xy11, xy12* *xy13, xy14, xy15*
	// *xy20, xy21, xy22* *xy23, xy24, xy25*
	// ****************** ******************
	public boolean checkSqueres() {
		for (int x = 0; x < sudokuArray.length; x++) {
			int[] temp = new int[9];
			int tempX = 0;
			for (int y = 0; y < sudokuArray[x].length; y++) {
				if (x == 0 && x == 3 && x == 6) {
					tempX = 0;
				} else if (x == 1 && x == 4 && x == 7) {
					tempX = 3;
				} else if (x == 2 && x == 5 && x == 8) {
					tempX = 6;
				}
				if (x < 3) {
					if (y < 3) {
						temp[y] = sudokuArray[tempX][y];
					} else if (6 > y && y >= 3) {
						temp[y] = sudokuArray[tempX + 1][y - 3];
					} else if (y >= 6) {
						temp[y] = sudokuArray[tempX + 2][y - 6];
					}

				} else if (6 > x && x >= 3) {
					if (y < 3) {
						temp[y] = sudokuArray[tempX][y + 3];
					} else if (6 > y && y >= 3) {
						temp[y] = sudokuArray[tempX + 1][y];
					} else if (y >= 6) {
						temp[y] = sudokuArray[tempX + 2][y - 3];
					}
				} else if (x >= 6) {
					if (y < 3) {
						temp[y] = sudokuArray[tempX][y + 6];
					} else if (6 > y && y >= 3) {
						temp[y] = sudokuArray[tempX + 1][y + 3];
					} else if (y >= 6) {
						temp[y] = sudokuArray[tempX + 2][y];
					}
				}
				
//				switch (x) {
//				case 0:
//					if (y < 3) {
//						temp[y] = sudokuArray[x][y];
//					} else if (6 > y && y >= 3) {
//						temp[y] = sudokuArray[x + 1][y - 3];
//					} else if (y >= 6) {
//						temp[y] = sudokuArray[x + 2][y - 6];
//					}
//					break;
//				case 1:
//					if (y < 3) {
//						temp[y] = sudokuArray[x + 2][y];
//					} else if (6 > y && y >= 3) {
//						temp[y] = sudokuArray[x + 1 + 2][y - 3];
//					} else if (y >= 6) {
//						temp[y] = sudokuArray[x + 2 + 2][y - 6];
//					}
//					break;
//				case 2:
//					if (y < 3) {
//						temp[y] = sudokuArray[x + 4][y];
//					} else if (6 > y && y >= 3) {
//						temp[y] = sudokuArray[x + 1 + 4][y - 3];
//					} else if (y >= 6) {
//						temp[y] = sudokuArray[x + 2 + 4][y - 6];
//					}
//					break;
//				case 3:
//					if (y < 3) {
//						temp[y] = sudokuArray[x - 3][y + 3];
//					} else if (6 > y && y >= 3) {
//						temp[y] = sudokuArray[x + 1 - 3][y];
//					} else if (y >= 6) {
//						temp[y] = sudokuArray[x + 2 - 3][y - 3];
//					}
//					break;
//				case 4:
//					if (y < 3) {
//						temp[y] = sudokuArray[x - 1][y + 3];
//					} else if (6 > y && y >= 3) {
//						temp[y] = sudokuArray[x + 1 - 1][y];
//					} else if (y >= 6) {
//						temp[y] = sudokuArray[x + 2 - 1][y - 3];
//					}
//					break;
//				case 5:
//					if (y < 3) {
//						temp[y] = sudokuArray[x + 1][y + 3];
//					} else if (6 > y && y >= 3) {
//						temp[y] = sudokuArray[x + 1 + 1][y];
//					} else if (y >= 6) {
//						temp[y] = sudokuArray[x + 2 + 1][y - 3];
//					}
//					break;
//				case 6:
//					if (y < 3) {
//						temp[y] = sudokuArray[x - 6][y + 6];
//					} else if (6 > y && y >= 3) {
//						temp[y] = sudokuArray[x + 1 - 6][y + 3];
//					} else if (y >= 6) {
//						temp[y] = sudokuArray[x + 2 - 6][y];
//					}
//					break;
//				case 7:
//					if (y < 3) {
//						temp[y] = sudokuArray[x - 4][y + 6];
//					} else if (6 > y && y >= 3) {
//						temp[y] = sudokuArray[x + 1 - 4][y + 3];
//					} else if (y >= 6) {
//						temp[y] = sudokuArray[x + 2 - 4][y];
//					}
//					break;
//				case 8:
//					if (y < 3) {
//						temp[y] = sudokuArray[x - 2][y + 6];
//					} else if (6 > y && y >= 3) {
//						temp[y] = sudokuArray[x + 1 - 2][y + 3];
//					} else if (y >= 6) {
//						temp[y] = sudokuArray[x + 2 - 2][y];
//					}
//					break;
//				}
			}
			test(temp);
			if (containsOneToNine(temp) == false) {
				return false;
			}
		}
		return true;
	}

	// xy00 xy01 xy02 xy03 ..
	// xy10 xy11 xy12 xy13 ..
	public boolean checkRows() {
		for (int x = 0; x < sudokuArray.length; x++) {
			int[] temp = new int[9];
			for (int y = 0; y < sudokuArray[x].length; y++) {
				temp[y] = sudokuArray[x][y];
			}
			test(temp);
			if (containsOneToNine(temp) == false) {
				return false;
			}
		}
		return true;
	}

	// xy00 xy10 xy20 xy30 ..
	// xy01 xy11 xy21 xy31 ..
	public boolean checkColums() {
		for (int x = 0; x < sudokuArray.length; x++) {
			int[] temp = new int[9];
			for (int y = 0; y < sudokuArray[x].length; y++) {
				temp[y] = sudokuArray[y][x];
			}
			test(temp);
			if (containsOneToNine(temp) == false) {
				return false;
			}
		}
		return true;
	}
	
	public boolean containsOneToNine(int[] temp) {
		int howManyGoodValues = 0;
		int value = 1;
		for (int j = 0; j < temp.length; j++) {
			for (int a = 0; a < temp.length; a++) {
				if (value == temp[a]) {
					value++;
					howManyGoodValues++;
				}
			}
		}
		if (howManyGoodValues == 9) {
			return true;
		} else {
			return false;
		}
	}
	
	public void test(int[] temp) {
		for (int i = 0; i < temp.length; i++) {
			System.out.print(temp[i] + ", ");
		}
		System.out.println();
	}
}
